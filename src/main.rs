extern crate hyper;

use hyper::rt::{self, Future};
use hyper::service::service_fn_ok;
use hyper::{Body, Request, Response, Server};

mod dynamic;
mod static_file;
fn route_request(request: Request<Body>) -> Response<Body> {
    match request.uri().path() {
        "/" => dynamic::handle_index(request),
        "/favicon.ico" => static_file::favicon(request),
        "/style.css" => static_file::style(request),
        _ => dynamic::handle_fetch(request),
    }
}

fn main() {
    pretty_env_logger::init();
    let addr = ([127, 0, 0, 1], 3000).into();
    let new_svc = || {
        // service_fn_ok converts our function into a `Service`
        service_fn_ok(route_request)
    };
    let server = Server::bind(&addr)
        .serve(new_svc)
        .map_err(|e| eprintln!("server error: {}", e));

    println!("Listening on http://{}", addr);

    rt::run(server);
}
