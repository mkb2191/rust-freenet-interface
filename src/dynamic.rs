extern crate hyper;
use hyper::{Body, Request, Response, StatusCode};

pub fn get_request_status(freenet_key: &str) -> (f32, u32) {
    if !freenet_key.contains("@") {
        return (0.0, 500);
    }
    (0.5, 200)
}

fn format_dynamic(title: &str, body: &str) -> String {
    format!(
        "<html><head><title>{} - Freenet</title><link rel='stylesheet' href='style.css'></head>
<body>
<div class='top'>
    <a href='/' id='home'>Home</a>
    <form action='/freenet' method='get'>
        Freesite URL: <input type='text' name='url'>
        <input type='submit' value='submit'>
    </form>
</div>
{}</body></html>",
        title, body
    )
}

pub fn handle_index(_: Request<Body>) -> Response<Body> {
    Response::new(Body::from(format_dynamic("Home", "")))
}

pub fn handle_fetch(request: Request<Body>) -> Response<Body> {
    let freenet_key = match request.uri().path() {
        "/" => request.uri().query().unwrap_or_default(),
        _ => request.uri().path(),
    };
    let (progress, status) = get_request_status(freenet_key);
    match status {
        500 => {
            let mut response = Response::new(Body::from(format_dynamic(
                "Invalid Key",
                "Invalid Key - No @ in URL",
            )));
            *response.status_mut() = StatusCode::BAD_REQUEST;
            response
        }
        200 => Response::new(Body::from(format_dynamic(
            "Home",
            &format!(
                "<div id='progress'><div style='width:{}%'></div></div>",
                progress * 100.0
            ),
        ))),
        _ => {
            let mut response = Response::new(Body::from(format_dynamic(
                freenet_key,
                &format!("Unknown status code returned: {}", status),
            )));
            *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
            response
        }
    }
}
