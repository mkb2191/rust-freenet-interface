extern crate hyper;

use hyper::{header, Body, Request, Response};

pub fn favicon(_: Request<Body>) -> Response<Body> {
    Response::new(Body::from(include_bytes!("favicon.ico").to_vec()))
}

pub fn style(_: Request<Body>) -> Response<Body> {
    let mut response = Response::new(Body::from(include_bytes!("style.css").to_vec()));
    response.headers_mut().append(
        header::CONTENT_TYPE,
        header::HeaderValue::from_str("text/css").unwrap(),
    );
    response
}
